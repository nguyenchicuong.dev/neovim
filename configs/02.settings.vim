syntax on

set encoding=UTF-8
set mouse=a

set incsearch
set hlsearch

set tabstop=4
set softtabstop=0
set shiftwidth=4
set showtabline=2

set number
set smarttab
set expandtab
set autoindent
set list
set listchars=tab:▸\ ,trail:·
set clipboard=unnamedplus
set cursorline
set nohlsearch
set showtabline=1

" Fold
set foldmethod=syntax
set foldnestmax=10
set nofoldenable
set foldlevel=2
