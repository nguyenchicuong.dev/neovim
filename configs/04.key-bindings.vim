"Tabpage
map <C-L> gt
map <C-H> gT

"COC
"" Map Ctrl + Space để show list functions/biến autocomplete
inoremap <silent><expr> <c-space> coc#refresh()
"" Tự động import file của biến/function khi chọn và nhấn Tab
inoremap <expr> <TAB> pumvisible() ? "\<C-y>" : "\<C-g>u\<TAB>"

"NERDTree
map <C-n> :NERDTreeFocus<CR>
map <C-b> :NERDTreeToggle<CR>
map <C-f> :NERDTreeFind<CR>

"FzF
nnoremap <silent> <C-p> :FZF<CR>
nnoremap <silent> <Leader>f :Rg<CR>

" Floar Terminal
nnoremap  <silent>  <C-T> :FloatermNew <CR>
nnoremap  <silent>  <C-S-H> :FloatermPrev <CR>
nnoremap  <silent>  <C-S-L> :FloatermNext <CR>
nnoremap  <silent>  <C-`> :FloatermToggle <CR>

" Terminal
tnoremap  <ESC> <C-\><C-n>  <CR>


nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>0 <Plug>AirlineSelectTab0
nmap <leader>[ <Plug>AirlineSelectPrevTab
nmap <leader>] <Plug>AirlineSelectNextTab

" Glance
nnoremap gR <CMD>Glance references<CR>
nnoremap gD <CMD>Glance definitions<CR>
nnoremap gY <CMD>Glance type_definitions<CR>
nnoremap gM <CMD>Glance implementations<CR>
