filetype plugin on
filetype plugin indent on

for f in split(glob('~/.config/nvim/configs/*.vim'), '\n')
   exe 'source' f
endfor
